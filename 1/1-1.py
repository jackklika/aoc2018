
freq = 0
f = open('in')
for ln in f:
    sign = ln[0:1]
    num = int(ln[1:])
    
    if sign == "+":
        freq += num
    elif sign == "-":
        freq -= num
    else:
        print("Error")
        exit(1)

print(freq)
