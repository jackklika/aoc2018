
freq = 0
freqset = set() # Sets are much faster than lists!!
f = open('in')

lines = f.read().split()

# Infinite loop until finding a duplicate
while True:

    # Iterate through all lines
    for ln in lines:
        sign = ln[0:1]
        num = int(ln[1:])
        
        # Adding/Subtracting to frequency
        if sign == "+":
            freq += num
        elif sign == "-":
            freq -= num
        else:
            print("Error")
            exit(1)

        if freq in freqset:
            print("Match found:", freq)
            exit(0)

        freqset.add(freq)

    print(len(freqset))
