import string

f = open('in')
lines = f.read().split()

twocnt = 0
threecnt = 0

# Iterate through all lines
for ln in lines:

    d = dict.fromkeys(string.ascii_lowercase, 0)
    for c in [*ln]: # split into chars
        d[c] += 1
    
    # iterate over keys, find counts of letters
    hastwo = False
    hasthree = False
    for l in d: # For dict
        if d[l] == 2: hastwo = True
        if d[l] == 3: hasthree = True

    if hastwo == True: twocnt += 1
    if hasthree == True: threecnt += 1
    print(ln, twocnt, threecnt)

print(twocnt, "*", threecnt, "=", twocnt*threecnt)
