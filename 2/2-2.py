# Answer is cvgywxqubnuaefmslkjdrpfzyi and cvgywxqubnuaefmsldjdrpfzyi, so cvgywxqubnuaefmsljdrpfzyi
# I found this by piping this all into vim and searching for incrementally higher numbers
# ...because sometimes it's easier to do this than write more code :)

import string

f = open('in')
lines = f.read().split()

compdict = {}

# Iterate through all lines
# Data Structure: compdict["string"] = a dictionary of "otherstrings" that point to integers that are the character differences
for ln in lines:

    # tmplist is the strings that will be compared with
    tmplist = []
    tmplist = lines.copy()
    tmplist.remove(ln)

    valdict = {}

    for comp in tmplist: # for every string to be compared with this...
        score = 0
        for n in range(len(comp)): # for each character in the strings...
            if [*comp][n] == [*ln][n]: # add score if they're the same
                score += 1
        print(ln, comp, score) 
        valdict[comp] = score
    compdict[ln] = valdict

