import numpy
import pandas

class Claim:
    def __init__(self, num, cx, cy, sx, sy):
        self.num = int(num)
        self.cx = int(cx)
        self.cy = int(cy)
        self.sx = int(sx)
        self.sy = int(sy)
    def __str__(self):
        return "#%s (%s,%s) to (%s,%s)" % (self.num, self.cx, self.cy, self.sx, self.sy)

# Read file into "lines"
f = open('in')
lines = f.readlines()
lines = [x.strip() for x in lines] # strip \n

# Load all items into claimlist
claimlist = []
for ln in lines:
    tmp = (ln.split(" "))
    num = int(tmp[0][1:])
    cx = int(tmp[2].split(",")[0])
    cy = int(tmp[2].split(",")[1][:-1])
    sx = int(tmp[3].split("x")[0])
    sy = int(tmp[3].split("x")[1])
    
    claim = Claim(num, cx, cy, cx+sx, cy+sy)
    claimlist.append(claim)

print("Loaded %s claims." % len(claimlist))

# Build the grid, filled with zeros
grid = numpy.zeros(shape=(max(c.sy for c in claimlist), max(c.sx for c in claimlist))) # height, length
print("Created empty grid of size", grid.shape)

for c in claimlist:
    for x in range(c.cx-1, c.sx-1):
        for y in range(c.cy-1, c.sy-1):
            grid[x][y] += 1

print("Generating matrix.csv...")
dataframe = pandas.DataFrame(data=grid.astype(float))
dataframe.to_csv('matrix.csv', sep=' ', header=False, float_format='%.0f', index=False)

overlapsum = 0
for n in range(0, int(grid.max())+1):
    count = (grid == n).sum()
    print(count, "squares with overlap", n)
    if n > 1: overlapsum += count

print("Overlap sum (>=2):", overlapsum)
