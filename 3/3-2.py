import numpy
import pandas

class Claim:
    def __init__(self, num, cx, cy, sx, sy, cl):
        self.num = int(num) # ID number
        self.cx = int(cx) # Starting x coordinate
        self.cy = int(cy) # Starting y coordinate
        self.sx = int(sx) # Ending x coordinate
        self.sy = int(sy) # Ending y coordinate
        self.cl = cl # list of coordinates covered by (cx,cy) to (sx,sy)
    def __str__(self):
        return "#%s (%s,%s) to (%s,%s)" % (self.num, self.cx, self.cy, self.sx, self.sy)

# Read file into "lines"
f = open('in')
lines = f.readlines()
lines = [x.strip() for x in lines] # strip \n

# Load all items into claimlist
claimlist = []
for ln in lines:
    tmp = (ln.split(" "))
    num = int(tmp[0][1:])
    cx = int(tmp[2].split(",")[0])
    cy = int(tmp[2].split(",")[1][:-1])
    sx = int(tmp[3].split("x")[0])+cx
    sy = int(tmp[3].split("x")[1])+cy

    # Create list of covered coordinates for each Claim object
    cl = []
    for x in range(cx-1, sx-1):
        for y in range(cy-1, sy-1):
            cl.append((x, y))
    
    claim = Claim(num, cx, cy, cx, cy, cl)
    claimlist.append(claim)

print("Loaded %s claims." % len(claimlist))

# Comparing coordiante lists
print("Processing claim overlap...")
olist = claimlist.copy()
for c in claimlist:
    for cg in claimlist:
        #print(c.num, "&", cg.num, bool(set(c.cl) & set(cg.cl))) # debug
        if c.num != cg.num and bool(set(c.cl) & set(cg.cl)):
            olist.remove(c)
            break

print("Non-covered:", olist[0])
