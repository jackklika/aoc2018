import datetime
from parse import *

class Shift:
    def __init__(self, date, num, schedule):
        self.date = date # not really needed but might be useful later
        self.num = num
        self.schedule = schedule # array from 0 to 59, True if awake or present
    def __str__(self):
        return "%s -- #%s: %s" % (self.date, self.num, self.schedule)

# Read file into "lines"
f = open('in')
lines = f.readlines()
lines = [x.strip() for x in lines] # strip \n

shiftdict = {}

for ln in lines:
    if "begins shift" in ln:

        # Parse object and create Shift object
        out = parse("[{pdate} {ptime}] Guard #{pnum} begins shift", ln)
        
        date = out['pdate']

        if int(out['ptime'][:2]) == 23: # If the time is before midnight, make the key the next day
            date = ((parse("{:ti}", date)[0]) + datetime.timedelta(days=1)).strftime("%Y-%m-%d")

        shiftdict[date] = Shift(date, out['pnum'], [True] * 60)

        starttime = int(out['ptime'][-2]) # minutes
 
        #if starttime < 30: # What if a worker is late?
        #    for n in range(0, starttime): shiftdict[out['pdate']].schedule[n] = False
 
    elif "falls asleep" in ln:
        out = parse("[{pdate} {ptime}] falls asleep", ln)
        for n in range(int(out['ptime'][-2:]), 60):
            shiftdict[out['pdate']].schedule[n] = False
 
    elif "wakes up" in ln:
        out = parse("[{pdate} {ptime}] wakes up", ln)
        for n in range(int(out['ptime'][-2:]), 60):
            shiftdict[out['pdate']].schedule[n] = True



guarddict = {}
for s in shiftdict:
    if shiftdict[s].num not in guarddict: guarddict[shiftdict[s].num] = 0
    for n in range(0, 60):
        if shiftdict[s].schedule[n] == False: guarddict[shiftdict[s].num] += 1

for t in guarddict:
    print(t, guarddict[t])

print("MAX: GUARD #", max(guarddict, key=lambda k: guarddict[k]))
maxguardid = max(guarddict, key=lambda k: guarddict[k])

guardmin = {}
for t in guarddict:
    minarray = [0] * 60
    for s in shiftdict:
        if int(shiftdict[s].num) == int(t):
            for n in range(0, 60):
                if shiftdict[s].schedule[n] == False: minarray[n] += 1
    guardmin[t] = minarray

for t in guardmin:
    print(t, ":")
    for n in range(0, 60):
        print("\t", n, guardmin[t][n], " GG")
