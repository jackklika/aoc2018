import string

# Read file into poly
f = open('in')
poly = f.read().strip()

# Create list of letter combinations
combos = []
for c in "abcdefghijklmnopqrstuvwxyz":
    combos.append(c + c.upper())
    combos.append(c.upper() + c)

# Iterate through all combos, stopping if we reach the same length (no changes)
length = len(poly) + 1
while len(poly) < length:
    length = len(poly)
    for c in combos:
        poly = poly.replace(c, "")
    print(length)

print("FINAL: ", len(poly))
