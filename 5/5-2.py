import string

# Read file into poly
f = open('in')
poly = f.read().strip()

# Create list of letter combinations
combos = []
for c in "abcdefghijklmnopqrstuvwxyz":
    combos.append(c + c.upper())
    combos.append(c.upper() + c)


resultdict = {}
for l in "ABCDEFGHIJKLMNOPQRSTUVWXYZ":
    tmppoly = poly.replace(l, "")
    tmppoly = tmppoly.replace(l.lower(), "")
 
    length = len(tmppoly) + 1
    while len(tmppoly) < length:
        length = len(tmppoly)
        for c in combos:
            tmppoly = tmppoly.replace(c, "")
    
    #print(l.upper(), "/", l, ":", len(tmppoly))
    resultdict[(l.upper() + l)] = len(tmppoly)

print("LOWEST:", resultdict[max(resultdict, key=resultdict.get)])
    
