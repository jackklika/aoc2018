class Space:
    def __init__(self, symb, x, y, turfarr):
        self.symb = symb
        self.x = int(x)
        self.y = int(y)
        self.turfarr = turfarr
    def __str__(self):
        return "[%s] (%s, %s): %s" % (self.symb, self.x, self.y, self.turfarr)


# Read file into "lines"
f = open('in')
lines = f.readlines()
lines = [x.strip() for x in lines] # strip \n
print(lines)

symb = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"

CYCLES = 2 # Keep this at two!
ADJUST = 30 # bounding on both sides -- 100 extra squares from furthest points

coorddict = {}
for i in range(0, len(lines)):
    t = lines[i].split(", ")
    coorddict[symb[i]] = Space(symb[i], t[0], t[1], [0]*CYCLES)


# Set coordinate region
max_x = max(coorddict[s].x for s in coorddict)+1
max_y = max(coorddict[s].y for s in coorddict)+1

min_x = min(coorddict[s].x for s in coorddict)-1
min_y = min(coorddict[s].y for s in coorddict)-1

for n in range(0, CYCLES):
    ADJUST += CYCLES*3
    
    grid_x = max_x + ADJUST*2
    grid_y = max_y + ADJUST*2
    
    grid = [[' '] * grid_y for i in range(grid_x)]
    print(len(grid), len(grid[0]))
    
    for x in range(0, grid_x):
        print(x)
        for y in range(0, grid_y):
            dist = []
            for c in coorddict:
                s = coorddict[c]
                dist.append([s.symb, abs(s.y+ADJUST - y) + abs(s.x+ADJUST - x)])
            mi = min(dist, key=lambda s: s[1])

            # search distlist for duplicates matching mi[1]
            cc = 0
            for d in dist:
                if d[1] == mi[1]:
                    cc += 1

            if cc > 1:
                grid[x][y] = "." 
            else: 
                coorddict[mi[0]].turfarr[n] += 1
                grid[x][y] = mi[0]
            
    
    for c in coorddict: # plot initial positions for visualization
        s = coorddict[c]
        grid[s.x+ADJUST][s.y+ADJUST] = '*'
    
    filename = "grid" + str(n)
    print("Generating visual representation at", filename)
    f = open(filename, 'w')
    for x in range(0, grid_x):
        t = ""
        for y in range(0, grid_y):
            t += grid[x][y]
        f.write(t + "\n")

for c in coorddict:
    if coorddict[c].turfarr[0] != coorddict[c].turfarr[1]: print(coorddict[c])
    
